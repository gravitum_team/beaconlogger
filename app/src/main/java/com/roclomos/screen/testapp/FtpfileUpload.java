package com.roclomos.screen.testapp;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
 

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

/**
 * A program that demonstrates how to upload files from local computer
 * to a remote FTP server using Apache Commons Net API.
 * @author www.codejava.net
 */


public class FtpfileUpload {
	
	
    public void uploadfile(String video ){
    	
    	
    	
    	

        String server = "66.147.244.240";
        int port = 21;
        String user = "demo@kustomerclub.com";
        String pass = "RoclomosDemo!";
 
        FTPClient ftpClient = new FTPClient();
        try {
 
            ftpClient.connect(server, port);
            ftpClient.login(user, pass);
            ftpClient.enterLocalPassiveMode();
 
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
 
            // APPROACH #1: uploads first file using an InputStream




            File firstLocalFile = new File("/storage/emulated/0/notes/logbeacon.txt");
 
            String firstRemoteFile = "/log beacon/"+video+".txt";//logbeacon.txt";
            InputStream inputStream = new FileInputStream(firstLocalFile);
 
            System.out.println("Start uploading first file");
            boolean done = ftpClient.storeFile(firstRemoteFile, inputStream);
            inputStream.close();
            if (done) {
                System.out.println("The first file is uploaded successfully.");
               // Toast.makeText(FtpfileUpload.this, "", Toast.LENGTH_SHORT).show();
            }

            File firstLocalFile1 = new File("/storage/emulated/0/notes/logbeaconstatistics.txt");

           String firstRemoteFile1 = "/log beacon1/"+video+"stats.txt";//logbeacon.txt";
            InputStream inputStream1 = new FileInputStream(firstLocalFile1);

            System.out.println("Start uploading first file");
            boolean done1 = ftpClient.storeFile(firstRemoteFile1, inputStream1);
            inputStream1.close();
            if (done1) {
                System.out.println("The second file is uploaded successfully.");
          //       Toast.makeText(FtpfileUpload.this, "", Toast.LENGTH_SHORT).show();
            }


            //   File firstLocalFile1 = new File("/mnt/usbdrive2/"+pic);
            
          //  String firstRemoteFile1 = "/upload/"+pic;
           // InputStream inputStream1 = new FileInputStream(firstLocalFile1);
 
            //System.out.println("Start uploading first file");
            //boolean done1 = ftpClient.storeFile(firstRemoteFile1, inputStream1);
            //inputStream1.close();
            //if (done1) {
              //  System.out.println("The first file is uploaded successfully.");
            //}
            
            
            
 /*
            // APPROACH #2: uploads second file using an OutputStream
            File secondLocalFile = new File("E:/Test/Report.doc");
            String secondRemoteFile = "test/Report.doc";
            inputStream = new FileInputStream(secondLocalFile);
 
            System.out.println("Start uploading second file");
            OutputStream outputStream = ftpClient.storeFileStream(secondRemoteFile);
            byte[] bytesIn = new byte[4096];
            int read = 0;
 
            while ((read = inputStream.read(bytesIn)) != -1) {
                outputStream.write(bytesIn, 0, read);
            }
            inputStream.close();
            outputStream.close();
 
            boolean completed = ftpClient.completePendingCommand();
            if (completed) {
                System.out.println("The second file is uploaded successfully.");
            }
 */
        } catch (IOException ex) {
            System.out.println("Error: " + ex.getMessage());
            Log.d("file upload",ex.getMessage());ex.printStackTrace();
        }
        catch (Exception e)
        {
            Log.d("file upload",e.getMessage());
            System.out.println(e.getMessage());
        }
        finally {
            try {
                if (ftpClient.isConnected()) {
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
 
}