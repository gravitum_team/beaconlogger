package com.roclomos.screen.testapp;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.os.Handler;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.support.multidex.MultiDex;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

//import android.text.format.Time;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.estimote.sdk.SystemRequirementsChecker;
import com.estimote.sdk.Utils;

import org.apache.commons.net.io.Util;
import org.apache.james.mime4j.field.datetime.DateTime;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {
    private BeaconManager beaconManager;
    private Region region;
    String content="";
    private static final Region ALL_ESTIMOTE_BEACONS = new Region("roclomos",null,null,null);// UUID.fromString("B9407F30-F5F8-466E-AFF9-25556B57FE6D"), 1697, 45961);
//private int MY_PERMISSIONS_REQUEST_READ_CONTACTS=1234;
    String path;
    private void writeToFile(String data) {
        try
        {
            File root = new File(Environment.getExternalStorageDirectory(), "Notes");
            if (!root.exists()) {
                root.mkdirs();
            }

            File gpxfile = new File(root, "logbeacon.txt");
           path=gpxfile.getPath();

            //OutputStreamWriter osw = new OutputStreamWriter(gpxfile.)
            FileWriter writer = new FileWriter(gpxfile,true);

            writer.append(data);
            writer.flush();
            writer.close();
        //    Toast.makeText(this, gpxfile.getPath(), Toast.LENGTH_SHORT).show();
        }
        catch(IOException e)
        {  Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
            //importError = e.getMessage();
            //iError();
        }

    }
    String conn="";
    private void writestatistics(String dta) {
conn=dta;





        try
        {
            File root = new File(Environment.getExternalStorageDirectory(), "Notes");
            if (!root.exists()) {
                root.mkdirs();
            }

            File gpxfile = new File(root, "logbeaconstatistics.txt");
            path=gpxfile.getPath();
String data="Mean,Std,0-10,10-20,20-30,>30\n";
            //OutputStreamWriter osw = new OutputStreamWriter(gpxfile.)
            FileWriter writer = new FileWriter(gpxfile,true);

            writer.append(dta);
            writer.flush();
            writer.close();
            //    Toast.makeText(this, gpxfile.getPath(), Toast.LENGTH_SHORT).show();
        }
        catch(IOException e)
        {  Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
    //        e.printStackTrace();
            //importError = e.getMessage();
            //iError();
        }

    }




    private void clearFile(String data) {
        try
        {
            File root = new File(Environment.getExternalStorageDirectory(), "Notes");
            if (!root.exists()) {
                root.mkdirs();
            }

            File gpxfile = new File(root, "logbeacon.txt");
            if(!gpxfile.exists())
                return;
            path=gpxfile.getPath();

            //OutputStreamWriter osw = new OutputStreamWriter(gpxfile.)
            FileWriter writer = new FileWriter(gpxfile);

            writer.append("");
            writer.flush();
            writer.close();
            //Toast.makeText(this, gpxfile.getPath(), Toast.LENGTH_SHORT).show();
        }
        catch(IOException e)
        {  Toast.makeText(this, "error", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
            //importError = e.getMessage();
            //iError();
        }

    }

List<Double> distance =new ArrayList<Double>();



boolean showalert=false;
    int i=0, slidingcount=0;

    public void setupUI(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if(!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard();
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView);
            }
        }
    }

    public  void hideSoftKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager)  this.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
    }

String teststr;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        MultiDex.install(this);

        super.onCreate(savedInstanceState);
        mHandler = new Handler();

        setContentView(R.layout.activity_main);
        setupUI(findViewById(R.id.cont));
     /*   if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED){
            // Should we show an explanation?
            // No explanation needed, we can request the permission.
            //Toast.makeText(this,"wrong",Toast.LENGTH_LONG).show();
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    1234);
         //   return;
            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
            // app-defined int constant. The callback method gets the
            // result of the request.
        }



        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.INTERNET)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?


            // No explanation needed, we can request the permission.

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.INTERNET},
                    1233);
            return;
            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
            // app-defined int constant. The callback method gets the
            // result of the request.
        }
*/
        Thread.setDefaultUncaughtExceptionHandler (new Thread.UncaughtExceptionHandler()
        {
            @Override
            public void uncaughtException (Thread thread, Throwable e)
            {
                handleUncaughtException (thread, e);
            }
        });

        //getMean();
        //getStdDev();
//startRepeatingTask();
        EditText edt1 = (EditText) findViewById(R.id.majoret);
        EditText edt2 = (EditText) findViewById(R.id.minoret);
        EditText edt3 = (EditText) findViewById(R.id.windowet);
        EditText edt4 = (EditText) findViewById(R.id.l1et);
        EditText edt5 = (EditText) findViewById(R.id.l2et);
        EditText edt6 = (EditText) findViewById(R.id.l3et);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if(preferences.contains("s1id"))
            edt1.setText(preferences.getString("s1id",""));
        if(preferences.contains("s2id"))
            edt2.setText(preferences.getString("s2id",""));
        if(preferences.contains("s3id"))
            edt3.setText(preferences.getString("s3id",""));
        if(preferences.contains("s4id"))
            edt4.setText(preferences.getString("s4id",""));
        if(preferences.contains("s5id"))
            edt5.setText(preferences.getString("s5id",""));
        if(preferences.contains("s6id"))
            edt6.setText(preferences.getString("s6id",""));


//        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
  //      if (!mBluetoothAdapter.isEnabled()){
    //      mBluetoothAdapter.enable();
     //   }
        clearFile("");
       // writeToFile("sadfasdf");
       // writeToFile("12312331");
        beaconManager= new BeaconManager(this);

        beaconManager.setBackgroundScanPeriod(
                1L, 15L);
        beaconManager.setForegroundScanPeriod(1L, 0);






        Timestamp ts = new Timestamp(System.currentTimeMillis());
      //
       // Toast.makeText(MainActivity.this, nameappend, Toast.LENGTH_SHORT).show();
        region = new Region("ranged region",null, null, null);
        final EditText majet=(EditText)findViewById(R.id.majoret);
        final EditText minet=(EditText)findViewById(R.id.minoret);
        final TextView tv = (TextView)findViewById(R.id.textView);

        beaconManager.setRangingListener(new BeaconManager.RangingListener() {

            @Override public void onBeaconsDiscovered(Region region, List<Beacon> beacons) {

                //  if (!list.isEmpty()) {
                // Toast.makeText(MainActivity.this, list.size(), Toast.LENGTH_SHORT).show();

                for (Beacon b : beacons) {
teststr+="Found :"+b.getMajor()+","+b.getMinor()+"\n";


                    if ((b.getMajor() == Integer.parseInt(majet.getText().toString()) && b.getMinor() == Integer.parseInt(minet.getText().toString())))
                    {// ||(b.getMajor() == 64792 && b.getMinor() == 56446) ) {

                        i++;
                        tv.setText( "Number of Records Logged:"+i);
                        final int maj = Integer.parseInt(majet.getText().toString());
                        final int min = Integer.parseInt(minet.getText().toString());
                        // Toast.makeText(MainActivity.this, maj + min, Toast.LENGTH_SHORT).show();
                        String majs = "" + b.getMajor(), mins = "" + b.getMinor();
             //           if (majs.indexOf(majet.getText().toString()) < -1 || mins.indexOf(minet.getText().toString()) < -1)
               ///             continue;
                        //  if (b.getMinor() != min || b.getMajor() != maj)
                        //    continue;
                        if (showalert)
                            Toast.makeText(MainActivity.this, "Found Major:"+b.getMajor()+"\t Minor:"+b.getMinor()+"\tStarting Logging", Toast.LENGTH_SHORT).show();
                        showalert = false;
                        // String s;
                        Timestamp ts = new Timestamp(System.currentTimeMillis());
                        if(distance.size()>slidingcount)
                        distance.remove(slidingcount);
                        //Toast.makeText(MainActivity.this, slidingcount, Toast.LENGTH_SHORT).show();
                        distance.add(slidingcount, Utils.computeAccuracy(b));

                        mean.add(slidingcount, getMean());
                        sdv.add(slidingcount,getStdDev());
                        int ztot=0,ttot=0,ttoth=0,mtth=0;
                        for(double vals :distance )
                        {
                            if(vals<=lo1)
                                ztot++;
                            else if(vals>lo1&&vals<=lo2)
                                ttot++;
                            else if(vals>lo2&&vals<=lo3)
                                ttoth++;
                            else
                                mtth++;
                        }            //  ztotl.add(ztot);
                       // ttotl.add(ttot);
                       // ttothl.add(ttoth);
                       // mtthl.add(mtth);


                        double t4=((ztot)*100/ distance.size()),t5=((ttot)*100/ distance.size()),t6=((ttoth)*100/ distance.size()),t7=((mtth)*100/ distance.size());
                        String test4 = t4+"",test5 = t5+"",test6 = t6+"",test7 = t7+"";




                        if (i < window) {
                            writeToFile(maj + "\t" + min + "\t" + ts.toString() + "\t" + b.getRssi() + "\t" + Utils.computeAccuracy(b)+ "\t" + Utils.computeProximity(b) + "\t" + b.getMeasuredPower() + "\t0\t0\t0\t0\t0\t0" + "\n");
                            //            writestatistics(b.getMajor() + "\t" + b.getMinor() + "\t" + ts.toString() + "\t" + b.getRssi() + "\t" + Utils.computeAccuracy(b) + "\t" + Utils.computeProximity(b) + "\t" + b.getMeasuredPower() + "\t0\t0\t0\t0\t0\t0\t"+i+"\t"+slidingcount + "\n");
                        } else {
                            //String valsz=((ztot / window)+""),valst=((ttot / window)+""),valsth=((ttoth / window)+""),valsm=((mtth / window)+"");
                            //String z=valsz.indexOf(".")>0?valsz.split(".")[0]:valsz,t=valsz.indexOf(".")>0?valsz.split(".")[0]:valsz,tw=valst.indexOf(".")>0?valst.split(".")[0]:valst,th=valsth.indexOf(".")>0?valsth.split(".")[0]:valsth,mth=valsm.indexOf(".")>0?valsm.split(".")[0]:valsm;

                            writeToFile(maj + "\t" + min + "\t" + ts.toString() + "\t" + b.getRssi() + "\t" + Utils.computeAccuracy(b)+ "\t" + Utils.computeProximity(b) + "\t" + b.getMeasuredPower() + "\t" + String.format("%.2f", getMean()) + "\t" + String.format("%.2f", getStdDev()) + "\t" + t4 + "\t" + t5+ "\t" + t6+ "\t" + t7+ "\n");
                            //          writestatistics(b.getMajor() + "\t" + b.getMinor() + "\t" + ts.toString() + "\t" + b.getRssi() + "\t" + Utils.computeAccuracy(b) + "\t" + Utils.computeProximity(b) + "\t" + b.getMeasuredPower() + "\t" + String.format("%.2f", getMean())   + "\t" + String.format("%.2f", getStdDev())    + "\t" + ztot + "\t" + ttot + "\t" + ttoth + "\t" + mtth +"\t"+i+"\t"+slidingcount+"\n");
                        }
               //         content += b.getMajor() + "\t" + b.getMinor() + "\t" + ts.toString() + "\t" + b.getRssi() + "\t" + Utils.computeAccuracy(b) + "\t" + Utils.computeProximity(b) + "\t" + b.getMeasuredPower() + "\n";
                        // writeToFile(s);
                        //  Beacon nearestBeacon = list.get(0);
                        //  List<String> places = placesNearBeacon(nearestBeacon);
                        // TODO: update the UI here
                        //  Log.d("Airport", "Nearest places: " + places);

if(firsttime&&i>=window) {
    startRepeatingTask();
    firsttime=false;
}
                        if(i%window==0)//runs after every 1000 entries
                        {
                            slidingcount=0;//resets value so that the oldest values are over written

                        }

else
                        slidingcount++;

                    }

                }
                //  }
            }       });
    }

    public void handleUncaughtException (Thread thread, Throwable e)
    {

        // The following shows what I'd like, though it won't work like this.
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_EMAIL, "muruganandam8817@gmail.com");
        intent.putExtra(Intent.EXTRA_SUBJECT, "crash log");
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));
        intent.putExtra(Intent.EXTRA_TEXT,"window value:"+window+"\n"+ sw.toString());

        startActivity(Intent.createChooser(intent, "Send Email"));}



    long loopcount=0;
List<Double> mean=new ArrayList<Double>();
    List<Double> sdv=new ArrayList<Double>();
    List<Integer> ztotl=new ArrayList<Integer>();
    List<Integer> ttotl=new ArrayList<Integer>();
    List<Integer> ttothl=new ArrayList<Integer>();
    List<Integer> mtthl=new ArrayList<Integer>();
    double getMean()
    {
        //int loopc = i-prevsliding;
        double sum = 0.0;
        for(double vals : distance)
            sum += vals;
        return sum/distance.size();
    }

    double getVariance()
    {
      //  int loopc = i-prevsliding;
        double mean = getMean();
        double temp = 0;
        for(double vals : distance)
            temp += (mean-vals)*(mean-vals);
        return temp/distance.size();
    }

    double getStdDev()
    {
        return Math.sqrt(getVariance());
    }

    int prevsliding=0,resetv=0;
    public void updateStatus()
    {
       // if(i<1000)
         //   return;

        TextView edm=(TextView)findViewById(R.id.mean);
        edm.setText("Mean:"+String.format("%.2f", getMean())   );

        TextView edsd=(TextView)findViewById(R.id.sd);
        edsd.setText("SD:"+String.format("%.2f", getStdDev())   );
        TextView edhis=(TextView)findViewById(R.id.histogram);
        long ztot=0,ttot=0,ttoth=0,mtth=0;
        for(double vals :distance )
        {
            if(vals<=lo1)
                ztot++;
            else if(vals>lo1&&vals<=lo2)
                ttot++;
            else if(vals>lo2&&vals<=lo3)
                ttoth++;
            else
                mtth++;
        }
        double t4=((ztot)*100/ distance.size()),t5=((ttot)*100/ distance.size()),t6=((ttoth)*100/ distance.size()),t7=((mtth)*100/ distance.size());
        String test4 = t4+"",test5 = t5+"",test6 = t6+"",test7 = t7+"";


        edhis.setText("Histogram:0-"+lo1+":"+t4+"\t"+lo1+"-"+lo2+":"+t5+"\t"+lo2+"-"+lo3+":"+t6+"\t>"+lo3+":"+t7);


    }


    @Override protected void onDestroy() {
        beaconManager.disconnect();

        super.onDestroy();
    }

    private int mInterval = 1000; // 5 seconds by default, can be changed later
    private Handler mHandler;
    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            updateStatus();
         //   loopcount++;//this function can change value of mInterval.
            mHandler.postDelayed(mStatusChecker, mInterval);
        }
    };

    void startRepeatingTask() {

        // Toast.makeText(MainActivity.this, "Started the repeat function", Toast.LENGTH_SHORT).show();
        mStatusChecker.run();
    }

    void stopRepeatingTask() {
        mHandler.removeCallbacks(mStatusChecker);
    }





    String filename;



boolean firsttime=true;
int window=0;
    int lo1=0,lo2=0,lo3=0;
    public void startlog(View v)
{
    clearFile("");
    final EditText majet=(EditText)findViewById(R.id.majoret);
    final EditText minet=(EditText)findViewById(R.id.minoret);
    final EditText windowet=(EditText)findViewById(R.id.windowet);
    final EditText l1=(EditText)findViewById(R.id.l1et);
    final EditText l2=(EditText)findViewById(R.id.l2et);
    final EditText l3=(EditText)findViewById(R.id.l3et);

    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
    SharedPreferences.Editor editor = preferences.edit();
    editor.putString("s1id", majet.getText().toString());
    editor.putString("s2id", minet.getText().toString());
    editor.putString("s3id", windowet.getText().toString());

    editor.putString("s4id", l1.getText().toString());
    editor.putString("s5id", l2.getText().toString());
    editor.putString("s6id", l3.getText().toString());

   // editor.putString("s3id", edt3.getText().toString());
 //   if(cbx.isChecked())
   //     editor.putString("s4id", "true");
    // else
    //    editor.putString("s4id", "false");
    editor.commit();
    if (majet.getText().toString().isEmpty() || minet.getText().toString().isEmpty() || windowet.getText().toString().isEmpty() || l1.getText().toString().isEmpty()||l2.getText().toString().isEmpty()|| l3.getText().toString().isEmpty())
    {
        Toast.makeText(MainActivity.this, "Please enter values for major, minor ids and window", Toast.LENGTH_SHORT).show();
        return;
    }
    window=Integer.parseInt(windowet.getText().toString());
    Toast.makeText(MainActivity.this,"Window Size:"+ window, Toast.LENGTH_SHORT).show();
    lo1=Integer.parseInt(l1.getText().toString());
    lo2=Integer.parseInt(l2.getText().toString());lo3=Integer.parseInt(l3.getText().toString());

    showalert=true;
    filename=majet.getText().toString()+"-"+minet.getText().toString();
    Toast.makeText(MainActivity.this, "Searching for beacon: Major:"+majet.getText().toString()+"\t Minor:"+minet.getText().toString(), Toast.LENGTH_SHORT).show();
    beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
        @Override
        public void onServiceReady() {
            beaconManager.startRanging(ALL_ESTIMOTE_BEACONS);
            writestatistics("connected");
        }
    });
   //tesfunction();
}

    public void stoplog(View v) throws RemoteException {

        beaconManager.stopRanging(ALL_ESTIMOTE_BEACONS);
        stopRepeatingTask();
        //writestatistics();
        Toast.makeText(MainActivity.this, "Stopped Ranging", Toast.LENGTH_SHORT).show();
        Toast.makeText(MainActivity.this, "Saving Log on Server", Toast.LENGTH_SHORT).show();
        //writeToFile(content);
        content="";
        showalert=false;
        Thread th= new Thread(new Runnable(){
        @Override
        public void run() {
            FtpfileUpload ftp = new FtpfileUpload();
          //  DateTime dt = new DateTime();
            Timestamp ts = new Timestamp(System.currentTimeMillis());
            String time= ts.toString().split(" ")[1];

            String nameappend = time.split(":")[0]+time.split(":")[1]+time.split(":")[2];
            nameappend=nameappend.replace(".","");
            ftp.uploadfile(filename+nameappend);
           // Toast.makeText(MainActivity.this, "File saved to server", Toast.LENGTH_SHORT).show();
        }
    });
        th.start();


    }
    @Override
    protected void onResume() {
        super.onResume();



       if( SystemRequirementsChecker.checkWithDefaultDialogs(this))
       {

       }


    }

    @Override
    protected void onStart() {
        super.onStart();

        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                beaconManager.startRanging(region);
            }
        });
    }
    @Override
    protected void onPause() {


        super.onPause();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    protected void onStop() {
        beaconManager.disconnect();

        super.onStop();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }






   /* public void tesfunction()
    {
        {
        final EditText majet=(EditText)findViewById(R.id.majoret);
        final EditText minet=(EditText)findViewById(R.id.minoret);
        final TextView tv = (TextView)findViewById(R.id.textView);
        for(int g=0;g<=(window*3);g++) {
            i++;
            tv.setText("Number of Records Logged:" + i);
            final int maj = Integer.parseInt(majet.getText().toString());
            final int min = Integer.parseInt(minet.getText().toString());
            // Toast.makeText(MainActivity.this, maj + min, Toast.LENGTH_SHORT).show();
            String majs = "" + maj, mins = "" + min;
            //           if (majs.indexOf(majet.getText().toString()) < -1 || mins.indexOf(minet.getText().toString()) < -1)
            ///             continue;
            //  if (b.getMinor() != min || b.getMajor() != maj)
            //    continue;
            if (showalert)
                Toast.makeText(MainActivity.this, "Found Major:" + maj + "\t Minor:" + min + "\tStarting Logging", Toast.LENGTH_SHORT).show();
            showalert = false;
            // String s;
            Timestamp ts = new Timestamp(System.currentTimeMillis());
            if (distance.size() > slidingcount)
                distance.remove(slidingcount);
            distance.add(slidingcount, Math.random() + 3);

            mean.add(slidingcount, getMean());
            sdv.add(slidingcount, getStdDev());
            int ztot=0,ttot=0,ttoth=0,mtth=0;
            for(double vals :distance )
            {
                if(vals<=lo1)
                    ztot++;
                else if(vals>lo1&&vals<=lo2)
                    ttot++;
                else if(vals>lo2&&vals<=lo3)
                    ttoth++;
                else
                    mtth++;
            }            //  ztotl.add(ztot);
            // ttotl.add(ttot);
            // ttothl.add(ttoth);
            // mtthl.add(mtth);


            double t4=((ztot)*100/ window),t5=((ttot)*100/ window),t6=((ttoth)*100/ window),t7=((mtth)*100/ window);
            String test4 = t4+"",test5 = t5+"",test6 = t6+"",test7 = t7+"";

            String valsz=((int)(ztot/ window)+""),valst=((ttot / window)+""),valsth=((ttoth / window)+""),valsm=((mtth / window)+"");
            String z=valsz.indexOf(".")>0?valsz.split(".")[0]:valsz,t=valst.indexOf(".")>0?valsz.split(".")[0]:valsz,tw=valst.indexOf(".")>0?valst.split(".")[0]:valst,th=valsth.indexOf(".")>0?valsth.split(".")[0]:valsth,mth=valsm.indexOf(".")>0?valsm.split(".")[0]:valsm;
            if(test4.indexOf(".")>0)
                test4 =test4.substring(0,test4.indexOf("."));
            if(test5.indexOf(".")>0)
                test5 =test5.substring(0,test5.indexOf("."));

            if(test6.indexOf(".")>0)
                test6 =test6.substring(0,test6.indexOf("."));

            if(test7.indexOf(".")>0)
                test4 =test7.substring(0,test7.indexOf("."));


            if (i < window) {
                writeToFile(maj + "\t" + min + "\t" + ts.toString() + "\t" + -55 + "\t" + Math.random() + 3 + "\t" + "Immediate" + "\t" + "2.333" + "\t0\t0\t0\t0\t0\t0" + "\n");
                //            writestatistics(b.getMajor() + "\t" + b.getMinor() + "\t" + ts.toString() + "\t" + b.getRssi() + "\t" + Utils.computeAccuracy(b) + "\t" + Utils.computeProximity(b) + "\t" + b.getMeasuredPower() + "\t0\t0\t0\t0\t0\t0\t"+i+"\t"+slidingcount + "\n");
            } else {
                //String valsz=((ztot / window)+""),valst=((ttot / window)+""),valsth=((ttoth / window)+""),valsm=((mtth / window)+"");
                //String z=valsz.indexOf(".")>0?valsz.split(".")[0]:valsz,t=valsz.indexOf(".")>0?valsz.split(".")[0]:valsz,tw=valst.indexOf(".")>0?valst.split(".")[0]:valst,th=valsth.indexOf(".")>0?valsth.split(".")[0]:valsth,mth=valsm.indexOf(".")>0?valsm.split(".")[0]:valsm;

                writeToFile(maj + "\t" + min + "\t" + ts.toString() + "\t" + -55 + "\t" + Math.random() + 3 + "\t" + "Immediate" + "\t" + "2.333" + "\t" + String.format("%.2f", getMean()) + "\t" + String.format("%.2f", getStdDev()) + "\t" + t4 + "\t" + t5+ "\t" + t6+ "\t" + t7+ "\n");
                //          writestatistics(b.getMajor() + "\t" + b.getMinor() + "\t" + ts.toString() + "\t" + b.getRssi() + "\t" + Utils.computeAccuracy(b) + "\t" + Utils.computeProximity(b) + "\t" + b.getMeasuredPower() + "\t" + String.format("%.2f", getMean())   + "\t" + String.format("%.2f", getStdDev())    + "\t" + ztot + "\t" + ttot + "\t" + ttoth + "\t" + mtth +"\t"+i+"\t"+slidingcount+"\n");
            }
            //         content += b.getMajor() + "\t" + b.getMinor() + "\t" + ts.toString() + "\t" + b.getRssi() + "\t" + Utils.computeAccuracy(b) + "\t" + Utils.computeProximity(b) + "\t" + b.getMeasuredPower() + "\n";
            // writeToFile(s);
            //  Beacon nearestBeacon = list.get(0);
            //  List<String> places = placesNearBeacon(nearestBeacon);
            // TODO: update the UI here
            //  Log.d("Airport", "Nearest places: " + places);

            if (firsttime && i >= window) {
                startRepeatingTask();
                firsttime = false;
            }
            if (i % window == 0)//runs after every 1000 entries
            {
                slidingcount = 0;//resets value so that the oldest values are over written

            } else
                slidingcount++;


        }      }

    }*/
}
